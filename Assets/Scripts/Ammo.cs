﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ammo : MonoBehaviour {
	public virtual bool Enemy { get; set; }

	[SerializeField]
	private int damage;
	public int Damage {
		get { return damage; }
	}

	[SerializeField]
	private float speed;
	public float Speed {
		get { return speed; }
	}
}

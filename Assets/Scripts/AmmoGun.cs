﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoGun : Ammo {

	private Rigidbody rb;

	public void Start () {
		rb = GetComponent<Rigidbody>();
		if (!rb) {
			Debug.LogError("Rigidbody component not found.");
		}
	}

	public void FixedUpdate () {
		var newPos = rb.position + transform.forward * Time.fixedDeltaTime * Speed;
		rb.position = newPos;
	}
}

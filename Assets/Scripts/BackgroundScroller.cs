﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundScroller : MonoBehaviour {

	public float speed = 1;
	private Vector3 startPosition;
	private float movement = 0;

	private static BackgroundScroller instanceRef;

	public void Awake() {
		if (instanceRef == null) {
			instanceRef = this;
			DontDestroyOnLoad(gameObject);
		} else {
			DestroyImmediate(gameObject);
		}
	}

	void Start() {
		startPosition = transform.position;
	}

	void Update() {
		movement = Mathf.Repeat(movement + speed * Time.deltaTime, transform.localScale.y);
		transform.position = startPosition + Vector3.back * movement;
	}
}

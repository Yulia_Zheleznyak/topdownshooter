﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageByAmmo : MonoBehaviour {

	private Health health;

	public void Start() {
		health = GetComponent<Health>();
		if (!health) {
			Debug.LogError("Health component not found.");
		}
	}

	public void OnTriggerEnter(Collider other) {
		var ammo = other.gameObject.GetComponent<Ammo>();
		if (!ammo || (GetComponent<Enemy>() && ammo.Enemy)) {
			return;
		}
		health.TakeDamage(ammo.Damage);
		Destroy(ammo.gameObject);
	}
}

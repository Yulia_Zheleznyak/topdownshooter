﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageByEnemy : MonoBehaviour {

	private Health health;

	public void Start() {
		health = GetComponent<Health>();
		if (!health) {
			Debug.LogError("Health component not found.");
		}
	}

	public void OnTriggerEnter(Collider other) {
		var enemy = other.gameObject.GetComponent<Enemy>();
		if (!enemy) {
			return;
		}
		health.TakeDamage(enemy.Damage);
	}
}

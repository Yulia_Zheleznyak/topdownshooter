﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageByPlayer : MonoBehaviour {

	private Health health;

	public void Start() {
		health = GetComponent<Health>();
		if (!health) {
			Debug.LogError("Health component not found.");
		}
	}

	public void OnTriggerEnter(Collider other) {
		if (!other.gameObject.GetComponent<PlayerController>()) {
			return;
		}
		health.TakeDamage(health.Value);
	}
}

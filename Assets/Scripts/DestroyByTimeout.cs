﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyByTimeout : MonoBehaviour {

	public float timeout;

	void Start () {
		StartCoroutine(DelayedDestroy());
	}

	IEnumerator DelayedDestroy () {
		yield return new WaitForSeconds(timeout);
		Destroy(gameObject);
	}
}

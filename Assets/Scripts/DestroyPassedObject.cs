﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyPassedObject : MonoBehaviour {

	public void OnTriggerEnter(Collider other) {
		Destroy(other.gameObject);
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {
	public delegate void DestroyDelegate();
	public event DestroyDelegate BeforeDestroy;

	[SerializeField]
	private float speed;
	public float Speed {
		get { return speed; }
	}

	[SerializeField]
	private int damage;
	public int Damage {
		get { return damage; }
	}

	public void OnDestroy() {
		BeforeDestroy();
	}
}

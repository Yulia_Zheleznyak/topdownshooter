﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAsteroid : Enemy {

	public float angularVelocity;

	private Rigidbody rb;
	private Vector3 rot;
	private Vector3 dir;

	public void Start () {
		rb = GetComponent<Rigidbody>();
		if (!rb) {
			Debug.LogError("Rigidbody component not found.");
		}
		dir = transform.forward;
		rot = Random.rotation.eulerAngles;
	}

//	public void Update () {
//		transform.Rotate(rot * Time.deltaTime * Speed);
//	}

	public void FixedUpdate() {
		rb.MovePosition(rb.position + dir * Time.fixedDeltaTime * Speed);
		rb.MoveRotation(rb.rotation * Quaternion.Euler(rot * Time.fixedDeltaTime * angularVelocity));
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShipEasy : Enemy {

	[SerializeField]
	private Weapon weapon;

	private Rigidbody rb;

	public void Start () {
		rb = GetComponent<Rigidbody>();
		if (!rb) {
			Debug.LogError("Rigidbody component not found.");
		}
	}

	public void FixedUpdate() {
		rb.MovePosition(rb.position + transform.forward * Time.fixedDeltaTime * Speed);
	}

	public void Update () {
		if (weapon) {
			weapon.Shoot();
		}
	}
}

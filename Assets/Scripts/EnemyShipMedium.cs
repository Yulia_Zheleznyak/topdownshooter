﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShipMedium : Enemy {

	[SerializeField]
	private Weapon weapon;
	[SerializeField]
	private Transform player;

	private Vector3 dirToPlayer;

	private Rigidbody rb;

	public void Start () {
		rb = GetComponent<Rigidbody>();
		if (!rb) {
			Debug.LogError("Rigidbody component not found.");
		}
		dirToPlayer = player ? (player.position - transform.position).normalized : transform.forward;
	}

	public void FixedUpdate() {
		rb.MovePosition(rb.position + dirToPlayer * Time.fixedDeltaTime * Speed);
	}

	public void Update () {
		if (weapon) {
			weapon.Shoot();
		}
	}
}

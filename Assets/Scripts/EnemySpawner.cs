﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EnemySpawner : MonoBehaviour {

	public float StageWarmup;
	public EnemyWave[] SpawnWaves;

	void Start () {
		StartCoroutine(Spawn());

	}

	private IEnumerator Spawn() {
		foreach (var wave in SpawnWaves) {
			yield return new WaitForSeconds(StageWarmup);
			yield return StartCoroutine(wave.StartWave());
		}
		SceneManager.LoadScene("GameWin", LoadSceneMode.Single);
	}
}

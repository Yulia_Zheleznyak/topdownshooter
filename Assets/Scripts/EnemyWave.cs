﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyWave : MonoBehaviour {

	public float duration;
	public float spawnInterval;
	public Enemy[] enemies;

	private float startTime;
	private int aliveEnemy = 0;

	public IEnumerator StartWave() {
		startTime = Time.time;
		while (startTime + duration > Time.time) {
			var e = CreateEnemy();
			Enemy.DestroyDelegate handler = null;
			handler = () => {
				aliveEnemy--;
				e.BeforeDestroy -= handler;
			};
			e.BeforeDestroy += handler;
			aliveEnemy++;
			yield return new WaitForSeconds(spawnInterval);
		}
		yield return StartCoroutine(WaitDestroyAllEnemies());
	}

	private IEnumerator WaitDestroyAllEnemies() {
		while (true) {
			yield return null;
			if (aliveEnemy == 0) {
				yield break;
			}
		}
	}

	private Enemy CreateEnemy() {
		int idx = Random.Range(0, enemies.GetLength(0)); // random number from enemies size

		var border = transform.localScale.x;
		var pos = transform.position;
		Vector3 position = new Vector3(Random.Range(pos.x - border * 0.5f, pos.x + border * 0.5f), pos.y, pos.z); //random position
		Quaternion rotation = Quaternion.Euler(0, 180.0f, 0);
		var enemy = Object.Instantiate(enemies[idx], position, rotation) as Enemy;
		enemy.gameObject.SetActive(true);
		return enemy;
	}
}

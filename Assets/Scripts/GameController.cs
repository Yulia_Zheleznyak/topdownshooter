﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {

	[SerializeField]
	private KeyCode pauseKey;

	[SerializeField]
	private RectTransform pausePanel;

	[SerializeField, Tooltip("GameOver occures when this health drop down to 0")]
	private Health playerHealth;

	[SerializeField]
	private float gameoverTimeout;

	public void Resume() {
		pausePanel.gameObject.SetActive(false);
		Time.timeScale = 1.0f;
	}

	public void Pause() {
		Time.timeScale = 0.0f;
		pausePanel.gameObject.SetActive(true);
	}

	public void Mute() {
		AudioListener.volume = 1 - AudioListener.volume;
	}

	public void Update () {
		if (playerHealth.Value <= 0) {
			StartCoroutine(GameOver());
			return;
		}
		if (Input.GetKeyDown(pauseKey)) {
			Pause();
		}
	}

	private IEnumerator GameOver() {
		yield return new WaitForSeconds(gameoverTimeout);
		SceneManager.LoadScene("GameOver", LoadSceneMode.Single);
	}
}
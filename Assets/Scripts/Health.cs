﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Health : MonoBehaviour {

	public delegate void HealthChangedDelegate();
	public event HealthChangedDelegate HealthChanged;

	public GameObject explosion;
	public int minHealth = 0;
	public int maxHealth = 100;

	[SerializeField]
	private int health;

	public int Value {
		get { return health; }
	}

	public void TakeHeal(int heal) {
		health = Mathf.Min(health + heal, maxHealth);
		if (HealthChanged != null) {
			HealthChanged();
		}
	}

	public void TakeDamage(int damage) {
		health = Mathf.Max(health - damage, minHealth);
		if (HealthChanged != null) {
			HealthChanged();
		}
		if (health <= 0) {
			Explode();
		}
	}

	private void Explode() {
		if (explosion) {
			Instantiate(explosion, transform.position, transform.rotation);
		}
		Destroy(gameObject);
	}
}

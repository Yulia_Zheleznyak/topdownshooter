﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthUI : MonoBehaviour {

	public Health health;
	private Text healthText;

	public void Start() {
		health.HealthChanged += HealthUpdate;
		healthText = GetComponent<Text>();
		if (!healthText) {
			Debug.LogError("Health component not found");
			return;
		}
		HealthUpdate();
	}

	public void OnDestroy() {
		health.HealthChanged -= HealthUpdate;
	}

	private void HealthUpdate()	{
		healthText.text = health.Value.ToString();
	}
}

﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuController : MonoBehaviour {
	public void StartGame() {
		SceneManager.LoadScene("Game", LoadSceneMode.Single);
	}

	public void ExitGame() {
#if UNITY_EDITOR
		UnityEditor.EditorApplication.isPlaying = false;
#else
		Application.Quit();
#endif
	}

	public void MainMenu(){ 
		SceneManager.LoadScene("MainMenu", LoadSceneMode.Single);	
	}
}
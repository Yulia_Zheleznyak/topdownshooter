﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {
	
	public float speed;
	public float leftBoundary;
	public float rightBoundary;

	[SerializeField]
	private Weapon weapon;

	private Rigidbody rb;

	public void Start()	{
		rb = GetComponent<Rigidbody>();
		if (!rb) {
			Debug.LogError("Rigidbody component not found.");
		}
	}

	public void FixedUpdate() {
		var input = Vector3.right * Input.GetAxis("Horizontal");
		var newPos = rb.position + input * Time.fixedDeltaTime * speed;
		newPos.x = Mathf.Min(rightBoundary, Mathf.Max(leftBoundary, newPos.x));
		rb.MovePosition(newPos);
	}

	public void Update() {
		if (Input.GetKey(KeyCode.Space)) {
			if (weapon) {
				weapon.Shoot();
			}
		}
	}
}

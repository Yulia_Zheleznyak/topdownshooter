﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour {

	public Ammo ammo;
	public float reloadTime;

	private float lastShootTime;

	[SerializeField]
	private bool enemy;

	private AudioSource audioSource;

	public void Start() {
		lastShootTime = -reloadTime;
		audioSource = GetComponent<AudioSource>();
		if (!audioSource) {
			Debug.Log("AudioSource component not found.");
		}
	}

	public void Shoot() {
		if (reloadTime + lastShootTime < Time.time) {
			Ammo clone = Instantiate(ammo, transform.position, transform.rotation);
			clone.Enemy = enemy;
			lastShootTime = Time.time;
			if (audioSource) {
				audioSource.Play();
			}
		}
	}

}
